# Formació de professorat

Mòdul formatiu adreçat al professorat que està participant del pilot Decidim a Secundària.
Aquest itinerari formatiu conta de:

## Sessions d'administració

1. Administració general. El taulell d'administració. Alta de participants.
2. La creació d'un procés. Els primers components
3. La dinamització d'un procés. Recollida i valoració de propostes.
4. Òrgans de participació

## Sessions de metodologia

6. 
7. 