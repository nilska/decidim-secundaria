# Mòdul 4: Taller de co-creació de processos

7 sessions de 55 min.

## Objectius del mòdul
- Crear processos participatius per presentar-los al Consell Escolar
- Proposar


## Requeriments

### Coneixements prèvis
Es demana que el grup hagi realitzat primer els mòduls d'admin

### Aptituds

## Resultat d'aprenentatge
- accedir a la plataforma com a administradora
- crear un procés i configurar-ne el comportament segons cada fase
- preveure accions de suport per a la dinamització del procés. 

## Sessions

### 1. Què puc decidir al meu institut?
La sessió inicial del taller de co-creació de processos està pensada per consensuar quins són els processos que es desenvoluparan al llarg de tot el taller.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Debat | 15 min   | A partir de la pregunta: *que puc canviar del meu institut* es proposta extreure els temes principals que preocupen a les participants sobre el dia a dia del seu institut. <br> S'ha de dibuixar conjuntament quins són els espais de participació i de decisió que actualment exiteien en el seu institut. <br>Cal que quedi clar quins temes estan dintre de la seva capacitat de decidir i quins no, a fi de no generar falses espectatives i fustració.  <br> Alguns grups poden escollir dinamitzar alguns dels òrgans de participacio detectats en l'organigrama.| Pissarra | Per parelles o grups de tres, escullen un tema per posar a debat amb tota la comunitat del centre. |
| Treball en grup | 30 min | Es busca defir cada procés a partir de les preguntes:<br> - com es diu el procés?<br> - què es vol decidir? (descripció breu)<br> - com es vol decidir (descripció llarga i fases) <br>- quines accions realitzaràs (selecció dels components) | Material de suport: checklist per crear processos | Propostes definides i preparades per pujar-les al decidim |
| Tancament | 10 | Avaluar l'estat dels processos plantejats, preveure si es poden començar a implementar al decidim a la següent sessió |  | Creació d'una llista amb els processos plantejats  | |



### 2. Implementar el meu procés
Les sessions d'implementació busquen ser un espai de creació col\·lectiva, on es doni un procés d'aprenentatge entre iguals.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min | Ronda ràpida per saber quins procesos s'estan treballant | Ordinadors per als alumnes | |
| Treball | 40 mim | Per grups de treball es creen i configuren els processos en el Decidim. Cada cop que es plantegin dubtes de com crear un component o configurar una acció, es demana al grup si algú altre de la classe els pot ajudar, i si no es sap com realtizar l'acció, s'explica a la pissarra i a tot el grup. <br> Es vol fomentar que es produeixi un aprenentatge entre iguals. | Un ordinador per cada grup d'estudiants | Processos llestos per fer-se públics |  
| Tancament | 5 min | Ronda ràpida per veure en quin punt està cada procés |  |  |  |

\* Per desenvolupar la implementació del procés pot ser necessari dues o tres sessions de 55 min., segons el ritme assolit per cada grup.




### 3. Disseny del pla de comunicació
Preveure com s'explicarà el procés a la resta de comunitat educativa.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | Un cop dissenyat el nostre procés, necessitem planificar com el volem donar a conèixer. <br> És imporant planificar totes les accions on hi volguem implicar a l'alumnat|  |  |
| Treball | 35 min | Planificar totes les accions de comunicació segons: <br>- què vull comunicar? <br> - a qui vull comunicar-ho? <br> - Com ho faré? <br> Tenint en compte els canals i el llenguatge que faré servir en cada un dels casos.| Graella pel disseny del pla de comunicació | Planificació de les accions comunicatives |
| Tancament | 10 min | Ronda ràpida per saber què han fet els diferents grups. |  |  |  |


### 4. Escollim els nostres processos

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | L'objectiu de la última sessió del bloc és consensuar entre tota la classe quins són els processos que es presentaran al Consell Escolar | Prèviament hem publicat un procés privat per la classe on hem penjat les diferents propostes de processos |  |
| Treball | 35 min | Presentació ràpida de cada un dels processos davant de tot el grup| |  |
| Tancament | 10 min | Priorització de les propostes fent servir la recollida de suports en el procés creat per la classe. |  | Priorització realizada entre tot el grup |  |
