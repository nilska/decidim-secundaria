# Mòdul 1: Drets digitals i programari lliure

XXh

## Objectius del mòdul
- Conèixer quins són els efectes de la nostra vida digital: què són els rastres digitals, quina conseqüència tenen per la nostra intimitat i la nostra privacitat.
- Entendre Internet com un lloc on desenvolupar-se com a persona.
- Dotar eines per fer-ho de manera segura.


## Requeriments

### Coneixements prèvis

### Aptituds

## Resultat d'aprenentatge
- Els alumnes són concients dels riscos d'interactuar amb plataformes digitals gestionades per les GAFAM
- Els alumnes s'interessen per altres maneres de participar en línia

## Sessions

### 1. Introducció: capitalisme digital i dades

En aquesta sessió introduïrem els conceptes clau sobre privacitat en l'esfera d'internet, situant-lo en un marc de capitalisme de dades.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 25 min  | Presentació a través de la qual introduïm diversos temes: <br> - Context on hi ha un augment dels espais digitals<br> - Entendre la tecnologia sobre les comunicacions com un valor estratègic <br>- també com un valor comercial <br> - quin paper tenen els estats en el control de les comunicacions?<br> - concpte de ciutadania digital i models democràtics| Presentació (link) | Despertar curiositat i dubtes sobre l'activitat digital dels i les pròpies joves. |
| Debat | 20 min | Posar en qüestió moltes de les activitats digitals dels joves, així com les eines i serveis que fem servir. | |  |
| Tancament | 10 min | Explicació dels objectius el curs: realitzar processos de participació fent servir la plataforma Decidim del propi centre. |  |  |  |




### 2. Què passa quan ens connectem a Internet?

Entrem a analitzar amb detall què passa quan una persona es connecta a Internet.
Sessió dinàmica per realitzar en un espai ampli o a l'aire lliure.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | Es preparen tantes targetes com persones realtizaran la dinàmica. Cada targeta és un element de la xarxa d'Internet. <br>Cada persona ha de saber què fa exactament el seu element. | Model de targetes |  |
| Privacitat | 15 min | Seguint el model de la EFF, expliquem què passa quan ens connectem a aqualsevol pàgina que requereixi autentificació http/https| [Esquema de les connexions](https://www.eff.org/pages/tor-and-https)|  |
| Anonimat | 15 min |Seguint el mateix model, expliquem opcions per anonimitzar la nostra activitat digital |[Esquema de les connexions](https://www.eff.org/pages/tor-and-https) | |
| Debat | 15 min | Et sens segura quan naveges per la xarxa? què et fa sentir segur i què et fa sentir insegur? |  | Introduir conceptes de privacitat i anonimat |



### 3. Els rastres digitals

Arran de la sessió anterior, es pot fer evident la necessitat de conèixer realment quines dades estem deixant cada vegada que ens connectem.

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | |  |  |
| Treball | 35 min | | |  |
| Tancament | 10 min |  |  |  |  |

